<div align=center><img src="/readme/images/know_logo1.png" width="590" height="212"/></div>

### 项目说明（如果对你有用，请点亮右上角的Star！）
 
 ### 系统演示
 ![展示图](/readme/images/yanshi.png "展示图.png")
    
 ### Web版地址
 - 教师端地址: <a target="_blank" href="https://demo.sdwanyue.com/teacher">https://demo.sdwanyue.com/teacher</a> 账号:13866666666 密码:123456
 - 后台地址: <a target="_blank" href="https://demo.sdwanyue.com/admin">https://demo.sdwanyue.com/admin</a> 账号: demo 密码: 123456
 - 后台仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/Wanyue-knowledge-payment-admin">点击此处</a>
 
  
 ### 项目介绍 
 万岳知识付费系统打造沉浸式学习体验，提升教学质量，还原真实课堂。知识付费功能包含热门精选、在线直播、付费视频、付费音频、付费阅读等营销功能，实现用户快速裂变。提高用户工作效率和收入是成为知识付费的刚需，可以从海量信息中寻找到适合自身的产品，利用碎片化时间和少许资金就能获得自己需要的信息。
 
 万岳知识付费深刻理解用户诉求，紧盯市场需求。帮助大家低成本高效率体验知识付费平台，以利用互联网让人们生活更美好为使命，精益求精，创新研发，为客户创造更多价值！
 * 所有使用到的框架或者组件都是基于开源项目，代码保证100%开源。
 * 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的知识付费系统。
 
 系统前端采用uni-app+socket.io+WebRtc核心技术, 接口采用PhalApi框架配合TP5.1框架ThinkCMF,系统功能如下:
 
 
 ### 功能展示
  ![展示图](/readme/images/img_1.png "展示图.png")
  ![展示图](/readme/images/img_2.png "展示图.png")
  ![展示图](/readme/images/img_3_1.png "展示图.png")
  ![展示图](/readme/images/img_4.png "展示图.png")
  
  ### 开源版使用须知
  
  - 允许用于个人学习、教学案例
  
  - 开源版不适合商用，商用请购买商业版
  
  - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负
  
  ### 商业合作
   * 如果你想使用功能更完善的知识付费系统，请联系QQ客服: 2415408120 获取专业版
   * 如果您想基于知识付费系统进行定制开发，我们提供有偿定制服务支持！
   * 其他合作模式不限，欢迎来撩！
   * 官网地址：[http://www.sdwanyue.com](http://www.sdwanyue.com)
                    
      
  ### 联系我们（加客服经理微信或QQ，免费获取sql脚本）
    
<div style='height: 130px'>
        <img class="kefu_weixin" style="float:left;" src="/readme/images/weixinlianxi.png" width="602" height="123"/>
        <div style="float:left;">
            <p>QQ：2415408120</p>
            <p>QQ群：605514618</p>
        </div>
    </div>
    <a target="_blank" href="https://jq.qq.com/?_wv=1027&k=D4SY3XFY"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="万岳知识付费讨论群" title="万岳知识付费讨论群"></a> 可加
    
> QQ群：605514618
    
 QQ群入群需验证
 
 问题：万岳知识付费后台语言？
 
 答案：（提示：java or php）
    
![展示图](/readme/images/公众号.png "展示图.png")